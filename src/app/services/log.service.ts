import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";

import { Log } from "../models/Log";

@Injectable({
  providedIn: "root"
})
export class LogService {
  logs: Log[];

  // Un objet de classe "BehaviorSubject" doit lui être initialisé avec une valeur par défaut,
  // car la principale caractéristique est de toujours devoir retourner une valeur aux observateurs.
  // Lors de la souscription, l'objet retourne la dernière valeur qu'il possède.
  // Elle notifie ensuite les observateurs de la même manière qu'un observable.
  private logSource = new BehaviorSubject<Log>({
    id: null,
    text: null,
    date: null
  });

  // asObservable makes the original subject inaccessible for subscribers.
  // This way you can limit who can only subscribe and who can also emit values.
  selectedLog = this.logSource.asObservable();

  private stateSource = new BehaviorSubject<boolean>(true);
  stateClear = this.stateSource.asObservable();

  constructor() {
    // this.logs = [
    //   {
    //     id: "1",
    //     text: "Generated components",
    //     date: new Date("12/26/2017 12:54:23")
    //   },
    //   {
    //     id: "2",
    //     text: "Added Bootstrap",
    //     date: new Date("12/27/2017 09:33:13")
    //   },
    //   {
    //     id: "3",
    //     text: "Added logs component",
    //     date: new Date("12/27/2017 12:00:00")
    //   }
    // ];
    this.logs = [];
  }

  getLogs(): Observable<Log[]> {
    // Get logs from localStorage: must be returned as a JSON object
    if (localStorage.getItem("logs") === null) {
      this.logs = [];
    } else {
      this.logs = JSON.parse(localStorage.getItem("logs"));
    }
    return of(this.logs);
  }

  // cette méthode permet de subscribe au log sur lequel on vient de cliquer,
  //  et on pourra faire l'update du texte ds l'input
  setFormLog(log: Log) {
    this.logSource.next(log);
  }

  addLog(log: Log) {
    this.logs.unshift(log);
    // Add to localStorage: must be stringified before
    localStorage.setItem("logs", JSON.stringify(this.logs));
    return of(
      this.logs.sort((a, b) => {
        return b.date - a.date;
      })
    );
  }

  updateLog(log: Log) {
    this.logs.forEach((current, index) => {
      if (current.id == log.id) {
        this.logs.splice(index, 1); // splice retire du tableau '1' élément à partir de la place 'index'
      }
    });
    this.logs.unshift(log);
    // Update localStorage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  deleteLog(log: Log) {
    this.logs.forEach((current, index) => {
      if (current.id == log.id) {
        this.logs.splice(index, 1); // splice retire du tableau '1' élément à partir de la place 'index'
      }
    });
    // Delete localStorage
    localStorage.setItem("logs", JSON.stringify(this.logs));
  }

  clearState() {
    this.stateSource.next(true);
  }
}
